import "tailwindcss/tailwind.css";

import { ThemeProvider } from "next-themes";
import React from "react";

import type { AppProps /*, AppContext */ } from "next/app";

const MyApp = ({ Component, pageProps }: AppProps): JSX.Element => {
  return (
    <ThemeProvider attribute="class" defaultTheme="system">
      <Component {...pageProps} />
    </ThemeProvider>
  );
};

export default MyApp;
