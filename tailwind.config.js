module.exports = {
  purge: ["./pages/**/*.tsx", "./components/**/*.tsx", "./astrology/**/*.tsx"],
  darkMode: "class", // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
