import React from "react";

import { 各t惑星が各n惑星に対して取る各月のアスペクト } from "./カスタムフック";
import { 閲覧テーブル本文 } from "./閲覧テーブル本文";
import { 閲覧テーブル見出し行 } from "./閲覧テーブル見出し行";

type 引数 = {
  各t惑星が各n惑星に対して取る各月のアスペクト: 各t惑星が各n惑星に対して取る各月のアスペクト;
};

export const 閲覧エリア: React.FC<引数> = (引数) => {
  return (
    <>
      <section>
        <h2 className="text-3xl mt-16 leading-normal text-black dark:text-gray-100">
          トランジットの各星がとるアスペクト
        </h2>

        <section>
          <h3 className="text-2xl mt-10 text-black dark:text-gray-100">木星</h3>
          <div className="mt-6 overflow-x-auto relative">
            <div style={{ width: "fit-content" }}>
              <閲覧テーブル見出し行 />
              <閲覧テーブル本文
                特定のt惑星が各n惑星に対して取る各月のアスペクト={
                  引数.各t惑星が各n惑星に対して取る各月のアスペクト.木星
                }
              />
            </div>
          </div>
        </section>

        <section>
          <h3 className="text-2xl mt-12 text-black dark:text-gray-100">土星</h3>
          <div className="mt-6 overflow-x-auto relative">
            <div style={{ width: "fit-content" }}>
              <閲覧テーブル見出し行 />
              <閲覧テーブル本文
                特定のt惑星が各n惑星に対して取る各月のアスペクト={
                  引数.各t惑星が各n惑星に対して取る各月のアスペクト.土星
                }
              />
            </div>
          </div>
        </section>

        <section>
          <h3 className="text-2xl mt-12 text-black dark:text-gray-100">
            天王星
          </h3>
          <div className="mt-6 overflow-x-auto relative">
            <div style={{ width: "fit-content" }}>
              <閲覧テーブル見出し行 />
              <閲覧テーブル本文
                特定のt惑星が各n惑星に対して取る各月のアスペクト={
                  引数.各t惑星が各n惑星に対して取る各月のアスペクト.天王星
                }
              />
            </div>
          </div>
        </section>

        <section>
          <h3 className="text-2xl mt-12 text-black dark:text-gray-100">
            海王星
          </h3>
          <div className="mt-6 overflow-x-auto relative">
            <div style={{ width: "fit-content" }}>
              <閲覧テーブル見出し行 />
              <閲覧テーブル本文
                特定のt惑星が各n惑星に対して取る各月のアスペクト={
                  引数.各t惑星が各n惑星に対して取る各月のアスペクト.海王星
                }
              />
            </div>
          </div>
        </section>

        <section>
          <h3 className="text-2xl mt-12 text-black dark:text-gray-100">
            冥王星
          </h3>
          <div className="mt-6 overflow-x-auto relative">
            <div style={{ width: "fit-content" }}>
              <閲覧テーブル見出し行 />
              <閲覧テーブル本文
                特定のt惑星が各n惑星に対して取る各月のアスペクト={
                  引数.各t惑星が各n惑星に対して取る各月のアスペクト.冥王星
                }
              />
            </div>
          </div>
        </section>
      </section>
    </>
  );
};
