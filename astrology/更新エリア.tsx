import { useRouter } from "next/router";
import React from "react";

import { 各n惑星の配置, 各n惑星の配置を更新 } from "./カスタムフック";
import { 更新入力欄 } from "./更新入力欄";

type 引数 = {
  各n惑星の配置: 各n惑星の配置;
  各n惑星の配置を更新: 各n惑星の配置を更新;
};

export const 更新エリア: React.FC<引数> = (引数) => {
  const ルーター = useRouter();

  const フォームの送信をハンドリング = React.useCallback(
    (イベント: React.FormEvent<HTMLFormElement>) => {
      イベント.preventDefault();
      ルーター.replace({
        query: {
          q: JSON.stringify(引数.各n惑星の配置),
        },
      });
    },
    [ルーター, 引数.各n惑星の配置]
  );

  return (
    <>
      <section>
        <h2 className="text-3xl mt-8 leading-normal text-black dark:text-gray-100">
          生まれの星の配置
        </h2>

        <form className="mt-4" onSubmit={フォームの送信をハンドリング}>
          <更新入力欄
            n惑星="太陽"
            n惑星の配置={引数.各n惑星の配置["太陽"]}
            各n惑星の配置を更新={引数.各n惑星の配置を更新}
          />
          <更新入力欄
            n惑星="月"
            n惑星の配置={引数.各n惑星の配置["月"]}
            各n惑星の配置を更新={引数.各n惑星の配置を更新}
          />
          <更新入力欄
            n惑星="水星"
            n惑星の配置={引数.各n惑星の配置["水星"]}
            各n惑星の配置を更新={引数.各n惑星の配置を更新}
          />
          <更新入力欄
            n惑星="金星"
            n惑星の配置={引数.各n惑星の配置["金星"]}
            各n惑星の配置を更新={引数.各n惑星の配置を更新}
          />
          <更新入力欄
            n惑星="火星"
            n惑星の配置={引数.各n惑星の配置["火星"]}
            各n惑星の配置を更新={引数.各n惑星の配置を更新}
          />
          <更新入力欄
            n惑星="木星"
            n惑星の配置={引数.各n惑星の配置["木星"]}
            各n惑星の配置を更新={引数.各n惑星の配置を更新}
          />
          <更新入力欄
            n惑星="土星"
            n惑星の配置={引数.各n惑星の配置["土星"]}
            各n惑星の配置を更新={引数.各n惑星の配置を更新}
          />

          <div className="mt-4">
            <button className="py-2 px-4 font-semibold rounded shadow-md text-white bg-green-500 dark:bg-green-600">
              更新する
            </button>
          </div>
        </form>
      </section>
    </>
  );
};
